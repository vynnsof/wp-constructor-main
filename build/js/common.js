$(document).ready(function () {
    // select2
    if ($(".select-menu-wrap").length) {
        $('.select-menu-wrap').select2({
            minimumResultsForSearch: -1,
            theme: "mobile-main-menu"
        });
    }
    //END select2

    //header scroll
    $(window).on("scroll", function () {
        headerChangingOnScroll();
    });

    function headerChangingOnScroll() {
        var headerHeight = $(".navbar-top");
        if ($(window).scrollTop() > headerHeight.height()) {
            headerHeight.addClass("navbar-scrolling");
        } else {
            headerHeight.removeClass("navbar-scrolling");
        }
    }

    headerChangingOnScroll();
//END header scroll

// add class on open header
    $('.navbar-top .navbar-toggler').on('click', function () {
        $(this).parents('.navbar-top').toggleClass('head-active-menu');
    });
//END add class on open header

    // faq
    $('.accordion').on('show.bs.collapse', function (e) {
        $(e.target).parent('.card').addClass('active');
    }).on('hide.bs.collapse', function (e) {
        $(e.target).parent('.card').removeClass('active');
    });
    //END faq


    // slider
    if ($('.owl-carousel').length > 0) {
        $('.owl-carousel').each(function () {
            $(this).owlCarousel({
                // autoplay: true,
                loop: true,
                nav: true,
                dots: true,
                margin: 20,
                responsiveClass: true,
                lazyLoad: true,
                autoWidth: true,
                responsive: {
                    0: {
                        autoWidth: false,
                        slideBy: 1,
                        items: 1
                    },
                    768: {
                        autoWidth: $(this).hasClass('slider-bottom') ? true : false,
                        slideBy: 1,
                        items: $(this).hasClass('slider-bottom') ? 1 : 2
                    },
                    1025: {
                        autoWidth: $(this).hasClass('slider-bottom') ? true : false,
                        // slideBy: $(this).hasClass('slider-bottom') ? 1 : 3,
                        slideBy: 1,
                        items: $(this).hasClass('slider-bottom') ? 1 : 3
                    }
                }

            });

            if ($(this).hasClass('testimonials-top')) {
                $(this).parents('.testimonials').find('h2').addClass('h2-imgs')
            }
        });
    }
    //END slider

    // scroll in tabs

    $(".custom-nav-text-reviews li.nav-item").on("click", function () {
        setTimeout(function () {
            $('.custom-text-reviews .scroll-wrapper').jScrollPane();
        }, 300)
    });

    $(".seo-tabs #menu-select").on("change", function () {
        setTimeout(function () {
            $('.custom-text-reviews .scroll-wrapper').jScrollPane();
        }, 300)
    });

    $(window).resize(function () {
        setTimeout(function () {
            $('.custom-text-reviews .scroll-wrapper').jScrollPane();
        }, 300)
    });
    //END scroll in tabs

    /*---Page testimonials---*/
    $('.custom-select-block').each(function () {
        $(this).on('change', function (e) {
            $(this).parents('section').find('.nav li a').eq($(this).val()).tab('show');
        });
    });
    // $('.menu-select-link').on('change', function (e) {
    //     window.location.href = $(this).parents().find('.nav li a').eq($(this).val()).attr('href');
    // });
    // if ($('.testimonials-container').length > 0) {
    //     $('.nav-tabs--vertical a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    //         var target = $(e.target).attr("href");
    //         if ((target == '#text-reviews')) {
    //             $('.testimonials-container-wrapper').show();
    //         } else {
    //             $('.testimonials-container-wrapper').hide();
    //         }
    //     });
    // }
    /*---END Page testimonials---*/

    CustomScroll(); // Custom Scroll

    // spoiler
    $(".btn_read_more").on("click", function () {
        $(".hidden_block").toggleClass("show");
        if (!$(this).hasClass("read_more_active")) {
            $(this).addClass("read_more_active");
        } else {
            $(this).removeClass("read_more_active");
        }
    });

    $(".hidden_block  .btn_read_more").on("click", function () {
        $(".btn_read_more").removeClass("read_more_active");
        $(this).addClass("read_more_active");
        let to_hide_block = $(".spoiler-wrap").offset().top;
        $("html, body").animate({scrollTop: to_hide_block - 150}, 1000);
    });
    //END spoiler

    // scroll to top
    $('.scrolltop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });
    //END scroll to top

    $(".dropdown-menu a").click(function () {
        $(this).parents(".dropdown").find('.btn').html($(this).text());
        $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
    });
});


/*-----Custom SCroll-----*/
function CustomScroll() {
    $(function () {
        $('.scroll-pane').jScrollPane();
    });
}

/*-----END Custom SCroll-----*/
