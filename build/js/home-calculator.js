//---- select2 + placeholder for search input
if ($(".js-example-basic-single").length) {

    var Defaults = $.fn.select2.amd.require('select2/defaults');

    $.extend(Defaults.defaults, {
        searchInputPlaceholder: ''
    });

    var SearchDropdown = $.fn.select2.amd.require('select2/dropdown/search');
    var _renderSearchDropdown = SearchDropdown.prototype.render;

    SearchDropdown.prototype.render = function(decorated) {
        // invoke parent method
        var $rendered = _renderSearchDropdown.apply(this, Array.prototype.slice.apply(arguments));
        this.$search.attr('placeholder', this.options.get('searchInputPlaceholder'));
        return $rendered;
    };

    $('.js-example-basic-single').select2({
        searchInputPlaceholder: 'Search'
    });
}
//---- END select2 + placeholder for search input

//----- datetimepicker
content_calculator = $('#calc_form');
function getMaxDate() {
    var defaultDatePicker = moment().add('month', 2)._d;
    return defaultDatePicker;
}

function getDefaultDeadline() {
    if (content_calculator.find('input[name="deadline_picker"]').val()) {
        return content_calculator.find('input[name="deadline_picker"]').val();
    }
    var defaultDatePicker = new Date();
    var defaultValueDeadline = content_calculator.find('input[name="deadline"]').val();

    if (defaultValueDeadline == 2135) {
        var defaultDatePicker = moment().add('hours', 3)._d;
    } else if (defaultValueDeadline == 2136) {
        var defaultDatePicker = moment().add('hours', 6)._d;
    } else if (defaultValueDeadline == 2137) {
        var defaultDatePicker = moment().add('hours', 12)._d;
    } else if (defaultValueDeadline == 2102) {
        var defaultDatePicker = moment().add('hours', 24)._d;
    } else if (defaultValueDeadline == 2103) {
        var defaultDatePicker = moment().add('days', 2)._d;
    } else if (defaultValueDeadline == 2104) {
        var defaultDatePicker = moment().add('days', 3)._d;
    } else if (defaultValueDeadline == 2108) {
        var defaultDatePicker = moment().add('days', 7)._d;
    } else if (defaultValueDeadline == 2139) {
        var defaultDatePicker = moment().add('days', 20)._d;
    } else {
        var defaultDatePicker = moment().add('days', 14)._d;
    }

    return defaultDatePicker;
}

function setBackgroudToPeriod() {
    //console.log('setBackgroudToPeriod');
    setTimeout(
        function () {
            $('.backgroud_period_deadline').removeClass('backgroud_period_deadline');

            var dateObj = new Date();
            var monthFrom = dateObj.getUTCMonth();
            var dayFrom = dateObj.getUTCDate();
            var yearFrom = dateObj.getUTCFullYear();

            //var currentDateTime = $('#datetimepicker').data("DateTimePicker").viewDate().toDate();
            var currentDateTime = $('#datetimepicker').data("DateTimePicker").date().toDate();
            var monthTo = currentDateTime.getMonth();
            var dayTo = currentDateTime.getDate();
            var yearTo = currentDateTime.getFullYear();
            //console.log('from', dayFrom, monthFrom, yearFrom,'to', dayTo,monthTo, yearTo );
            for (var y = yearFrom; y <= yearTo; y++) {
                for (var m = 0; m <= 11; m++) {
                    if ((y == yearFrom && m >= monthFrom) || (y == yearTo && m <= monthTo)) {
                        for (var d = 1; d <= 31; d++) {
                            var realMonthNumber = m + 1;
                            var element = $('[data-day="' + ((realMonthNumber) < 10 ? ('0' + realMonthNumber.toString()) : realMonthNumber) + '/' + (d < 10 ? ('0' + d.toString()) : d) + '/' + y + '"]');
                            if (!element.hasClass('disabled')) {
                                if (!((m == monthFrom && y == yearFrom && d == dayFrom) || (m == monthTo && y == yearTo && d == dayTo))) {
                                    if (m == monthTo && y == yearTo) {
                                        if (d < dayTo) {
                                            //console.log('day', ((realMonthNumber) < 10 ? ('0' + realMonthNumber.toString()) : realMonthNumber), d, '[data-day="' + realMonthNumber + '/' + (d < 10 ? ('0' + d.toString()) : d) + '/' + y + '"]');
                                            element.addClass('backgroud_period_deadline');
                                        }
                                    } else if (m == monthFrom && y == yearFrom) {
                                        if (d > dayFrom) {
                                            //console.log('day', ((realMonthNumber) < 10 ? ('0' + realMonthNumber.toString()) : realMonthNumber), d, '[data-day="' + (m + 1) + '/' + (d < 10 ? ('0' + d.toString()) : d) + '/' + y + '"]');
                                            element.addClass('backgroud_period_deadline');
                                        }
                                    } else if ((y == yearFrom && m > monthFrom && yearFrom != yearTo)
                                        || (y == yearTo && m < monthTo && yearFrom != yearTo)) {
                                        console.log('day middle', m, y, d);
                                        element.addClass('backgroud_period_deadline');
                                    } else if ((m < monthTo && m > monthFrom && yearFrom == yearTo)) {
                                        console.log('day new', m, y, d);
                                        element.addClass('backgroud_period_deadline');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        , 100);
}

function addTitleTimeLeft() {
    var date_current = new Date();
    var date_choose_by_client_object = moment($('#datetimepicker').data("DateTimePicker").date().toDate()).add(15, 'minute');
    var date_current_object = moment(date_current);
    var diff_in_seconds = date_choose_by_client_object.diff(date_current_object, 'seconds');

    if(Math.floor(diff_in_seconds/86400) > 0){
        var text = Math.floor(diff_in_seconds/86400) + ' days away';
    } else{
        var text = Math.floor(diff_in_seconds/3600) + ' hours away';
    }

    var theadBlockObjDays = $('#datetimepicker').parent().find('.bootstrap-datetimepicker-widget .datepicker-days .table-condensed thead');
    if(theadBlockObjDays.length){
        if (theadBlockObjDays.find('th.time-left').length) {
            theadBlockObjDays.find('th.time-left').html(text);
        } else {
            theadBlockObjDays.find('tr:first').after('<tr><th colspan="7" class="time-left">' + text + '</th></tr>');
        }
    }

    var theadBlockObjTime = $('#datetimepicker').parent().find('.bootstrap-datetimepicker-widget .timepicker-picker .table-condensed tbody');
    if (theadBlockObjTime.length) {
        if (theadBlockObjTime.find('td.time-left').length) {
            theadBlockObjTime.find('td.time-left').html(text);
        } else {
            theadBlockObjTime.find('tr:first').before('<tr><td colspan="7" class="time-left">' + text + '</td></tr>');
        }
    }
}

function addButtonConfirm(){
    $(".bootstrap-datetimepicker-widget").append(
        '<a class="datetimepicker-confirm" title="Confirm">Confirm</a>'
    );

    $(".bootstrap-datetimepicker-widget .datetimepicker-confirm").click(function () {
        $('#datetimepicker').data("DateTimePicker").hide();
    });
}

var minDateForDateimePicker = new Date();
minDateForDateimePicker.setSeconds(minDateForDateimePicker.getSeconds() + 3 * 60 * 60);
var format_datetimepicker_calculator = 'h:mm A, MMM DD';
$('#datetimepicker').datetimepicker({
    format: format_datetimepicker_calculator,
    useCurrent: false,
    //showClose: true,
    minDate: minDateForDateimePicker,
    maxDate: getMaxDate(),
    defaultDate: getDefaultDeadline(),
    ignoreReadonly: true,
    allowInputToggle: false,
    focusOnShow: true,
    viewMode: 'days',
    force12HoursFormat: true,
    customToggle: true,
    widgetPositioning:{
        horizontal: 'auto',
        vertical: 'bottom'
    }
    // debug: true
});

$('#datetimepicker').on("dp.show", function (e) {
    setBackgroudToPeriod();
    addTitleTimeLeft();
    addButtonConfirm();
    $(".deadline-button").click(function () {
        $(".deadline-button").removeClass('deadline-btn-active');
        $(this).addClass('deadline-btn-active');
    });
    $('.datepicker-days').on('click', '.day.active', function () {
        setBackgroudToPeriod();
    });
});

$('#datetimepicker').on("dp.change", function (e) {
    setBackgroudToPeriod();
    addTitleTimeLeft();
});

$('#datetimepicker').on("dp.update", function (e) {
    setBackgroudToPeriod();
    addTitleTimeLeft();
});

//----  END datetimepicker